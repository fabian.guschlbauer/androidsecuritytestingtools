# Meizodon
This project provides a Docker based setup of [Meizedon](https://github.com/Sebastiaan-Alvarez-Rodriguez/Meizodon/). 

# Start
`docker-compose run meizodon`

# Usage
Start Meizodon using `python3 /Meizodon/run.py`. 
An example apk to analyze is located at `/androidsecuritytestingtools/whatsapp.apk`